# Docker Nginx Proxy

This docker container provides an easy to set up nginx reverse proxy.

## Getting Started

1) Modify the following files with your server address:

- docker-compose.cert.yml
- etc/nginx/default.conf

2) Create a certificate:

```bash
docker-compose -f docker-compose.cert.yml up
```

Then Ctrl-C to stop container.

3) Start the container in daemon mode:

```bash
docker-compose up -d
```

## Renew Certificate

To renew the free Let's Encryp cerificate, stop the running container and run:

```bash
docker-compose -f docker-compose.cert.yml up
```

Then Ctrl-C to exit and restart the container.
